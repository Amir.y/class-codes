from datetime import datetime


def log_time(func):
    def is_positive(n):
        start = datetime.now()
        print("Start: {}".format(start))
        res = func(n)
        end = datetime.now()
        print("End: {}".format(end))
        print("Duration: {}".format(end - start))
        return res

    return is_positive
