
def string_maker(available_chars, string_length):
    """
    tools to generate a list of all available string on given length from given
    available characters
    :param available_chars: string, i.e: 'abc'
    :param string_length: length of preferred available strings in integer, i.e: 2
    :return: generate list, i.e: ['aa', 'ab', 'ac', ... , 'cb', 'cc']
    """
    if string_length == 1:
        return list(available_chars)
    return [i + j for i in available_chars for j in string_length(available_chars, string_length - 1)]
